*Warning*: This has been deprecated in favor of [spiro-network](https://gitlab.com/spirostack/spiro-network).

spiro-ip
========

A quick and simple module to get information about the various IPs of your salt
minions.

This is meant to be installed on minions.

Full docs available [online](https://spirostack.com/spiro-ip.html).

Installation
============

On minions, install the `spiro-ip` PyPI package.


Interface
=========

A number of things are provided:

Grains
------

* `externalip4`, `externalip6`: Queries external services for your IP, useful
  if the minion is behind a NAT or other complex network

Modules
-------

* `ip.addrs4`, `ip.addrs6`: Collates information about a minion's IP address
  from several sources. 

    * `network.ipaddrs` / `network.ipaddrs6`
    * AWS metadata if the [metadata grain](https://docs.saltstack.com/en/latest/ref/grains/all/salt.grains.metadata.html) is available
    * `externalip4` / `externalip6` grains (above)

Configuration
=============

spiro-ip requires no configuration to function.
